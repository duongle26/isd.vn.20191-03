# IDS - Group 3

# Final Project

## Lê Hải Dương

- Design Class/Package Diagram and Data Model
- Write source code for the whole project

## Nguyễn Anh Đức

- Update SDD
- Write test for One-way Ticket

## Lã Ngọc Dương

- Update SRS
- Write test for 24h Ticket

## Nguyễn Nhật Hải

- Write JavaDoc

---

# Contribution

## Lê Hải Dương - 40%

## Nguyễn Anh Đức - 20%

## Lã Ngọc Dương - 20%

## Nguyễn Nhật Hải - 20%

---

# Week2:

## Lê Hải Dương

- Luồng sự kiện cho usecase one-way ticket

## Nguyễn Anh Đức

- Luồng sự kiện cho usecase enter/exit platform

## Nguyễn Nhật Hải

- Luồng sự kiện cho usecase prepaidcard

## Lã Ngọc Dương

- Luồng sự kiện cho usecase 24h ticket

---

# Week3:

## Lê Hải Dương

### Design flow of events for 3 use case

- 1 Insert Ticket
- 2 Insert Card
- 3 Gate

### Hoàn thiện các phần còn thiếu trong tài liệu đặc tả

- 1 Giới thiệu
- 2 Mô tả tổng quan
- 3 Các yêu cầu khác

## Nguyễn Anh Đức

### Design flow of events for 3 use case

- 1 Check information of one-way tickets when enter station platform
- 2 Check information of prepaid card
- 3 Check information of 24h ticket

## Nguyễn Nhật Hải

### Quy trình nghiệp vụ

- Quy trình hoạt động của hệ thống quản lý vé / thẻ

## Lã Ngọc Dương

### Quy trình nghiệp vụ

- Quy trình sử dụng hệ thống của Passenger
- Quy trình hoạt động của hệ thống điều khiển cửa

---

# Week4:

## Lê Hải Dương

- Draw the sequence diagrams for: Enter the station platform using one-way ticket
- Re-design use-case diagram
- Summarize member's works

## Nguyễn Anh Đức

Design sequency diagram for use case

- Exit using one-way ticket

## Nguyễn Nhật Hải

Làm sequence diagram cho usecase:

- Enter using pre-paid card

- Exit using pre-paid card

## Lã Ngọc Dương

Làm sequence diagram cho usecase:

- Enter using 24h ticket

- Exit using 24h ticket

---

# Week5:

## Lê Hải Dương

### Add Combined Class Diagram for the whole system

### Update Sequence Diagram, add Communication Diagram and Class Diagram for use-case

- Enter the station platform using one-way ticket

## Nguyễn Anh Đức

### Update Sequence Diagram, add Communication Diagram and Class Diagram for use-case

- Exit the station platform using one-way ticket

## Nguyễn Nhật Hải

### Update Sequence Diagram, add Communication Diagram and Class Diagram for use-case

- Enter the station platform using prepaid card

- Exit the station platform using prepaid card

## Lã Ngọc Dương

### Update Sequence Diagram, add Communication Diagram and Class Diagram for use-case

- Enter the station platform using 24h ticket

- Exit the station platform using 24h ticket

---

# Week6:

## Lê Hải Dương

### Design Class Diagram for use-case

- Enter the station platform using one-way ticket

- The whole system

## Nguyễn Anh Đức

### Design Class Diagram for use-case

- Exit the station platform using one-way ticket

- Exit the station platform using 24h ticket

## Lã Ngọc Dương

### Design Class Diagram for use-case

- Enter the station platform using 24h ticket

## Nguyễn Nhật Hải

### Design Class Diagram for use-case

- Enter the station platform using prepaid card

- Exit the station platform using prepaid card

---

# Week7:

## Lê Hải Dương

### Conceptual Data Model: E-R Diagram

## Nguyễn Anh Đức

### Database Design

## Lã Ngọc Dương

### Database Design

## Nguyễn Nhật Hải

### Logical Data Model
