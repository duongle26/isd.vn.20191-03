# README

## Lê Hải Dương

### Add Combined Class Diagram for the whole system

### Update Sequence Diagram, add Communication Diagram and Class Diagram for use-case

- Enter the station platform using one-way ticket

## Nguyễn Anh Đức

### Update Sequence Diagram, add Communication Diagram and Class Diagram for use-case

- Exit the station platform using one-way ticket

## Nguyễn Nhật Hải

### Update Sequence Diagram, add Communication Diagram and Class Diagram for use-case

- Enter the station platform using prepaid card

- Exit the station platform using prepaid card

## Lã Ngọc Dương

### Update Sequence Diagram, add Communication Diagram and Class Diagram for use-case

- Enter the station platform using 24h ticket

- Exit the station platform using 24h ticket
