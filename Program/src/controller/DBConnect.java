package controller;

import java.sql.*;

/**
 * The Class DBConnect.
 */
public class DBConnect {

    private static Connection db;

    private static Statement stmt;

    static {
        try {
            db = new DBConnect().Connect();
            stmt = db.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets the db.
     *
     * @return the db
     */
    public static Connection getDb() {
        return db;
    }

    /**
     * Gets the stmt.
     *
     * @return the stmt
     */
    public static Statement getStmt() {
        return stmt;
    }

    /**
     * Connect.
     *
     * @return the connection
     */
    public Connection Connect() {

        Connection conn = null;
        try {
            String url = "jdbc:mariadb://localhost/AFC";
            String user = "root";
            String password = "123456";
            conn = DriverManager.getConnection(url, user, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conn;
    }
}