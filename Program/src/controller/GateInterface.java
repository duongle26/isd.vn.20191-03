package controller;
import hust.soict.se.gate.Gate;

/**
 * The Class GateInterface.
 */
public class GateInterface {

	private static GateInterface instance = new GateInterface();

	private Gate gate = Gate.getInstance();

	public static GateInterface getInstance() {
		return instance;
	}

	/**
	 * Open gate.
	 */
	protected void openGate() {
		gate.open();
	}

	/**
	 * Close gate.
	 */
	protected void closeGate() {
		gate.close();
	}
}