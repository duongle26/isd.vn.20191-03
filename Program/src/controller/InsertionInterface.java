package controller;

import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.scanner.CardScanner;
import hust.soict.se.recognizer.TicketRecognizer;

/**
 * The Class InsertionInterface.
 */
public class InsertionInterface {

	private static InsertionInterface instance = new InsertionInterface();

	public static InsertionInterface getInstance() {
		return instance;
	}
	
	/**
	 * Process ticket.
	 *
	 * @param barCode the barcode string
	 * @return the code of ticket
	 * @throws InvalidIDException when the barCode is invalid
	 */
	public String processTicket(String barCode) throws InvalidIDException {
		TicketRecognizer ticketRecognizer = TicketRecognizer.getInstance();
		return ticketRecognizer.process(barCode);
	}

	/**
	 * Process card.
	 *
	 * @param barCode the barcode string
	 * @return the code of card
	 * @throws InvalidIDException when the barCode is invalid
	 */
	public String processCard(String barCode) throws InvalidIDException {

		CardScanner cardScanner = CardScanner.getInstance();
		return cardScanner.process(barCode);
	}
}