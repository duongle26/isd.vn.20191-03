package controller;

import hust.soict.se.customexception.InvalidIDException;

import java.sql.SQLException;

/**
 * The Class PlatformController.
 */
public class PlatformController {

    private static PlatformController instance = new PlatformController();

    private StationController station = StationController.getInstance();

    private OneWayTicketController oneWayTicket = OneWayTicketController.getInstance();

    private Ticket24hController ticket24h = Ticket24hController.getInstance();
    
    private PrepaidCardController prepaidCard = PrepaidCardController.getInstance();
    
    private InsertionInterface insertion = InsertionInterface.getInstance();
    
    private GateInterface gate = GateInterface.getInstance();

    public static PlatformController getInstance() {
        return instance;
    }

    /**
     * Enter station.
     *
     * @param enterStationId the enter station id
     * @param method the method
     * @param barCode the barcode string
     * @throws InvalidIDException when the parameter is invalid
     * @throws SQLException if there was a problem querying the database
     */
    public void enterStation(int enterStationId, int method, String barCode) throws InvalidIDException, SQLException {
        String code = Character.isUpperCase(barCode.charAt(0)) ? insertion.processCard(barCode) : insertion.processTicket(barCode);
        switch (method) {
            case 1:
                if (oneWayTicket.enterStation(enterStationId, code)) this.openGate();
                else this.closeGate();
                break;
            case 2:
                if (ticket24h.enterStation(enterStationId, code)) this.openGate();
                else this.closeGate();
                break;
            case 3:
                if (prepaidCard.enterStation(enterStationId, code)) this.openGate();
                else this.closeGate();
                break;
        }
    }

    /**
     * Exit station.
     *
     * @param exitStationId the exit station id
     * @param method the method
     * @param barCode the barcode string
     * @throws InvalidIDException when the parameter is invalid
     * @throws SQLException if there was a problem querying the database
     */
    public void exitStation(int exitStationId, int method, String barCode) throws InvalidIDException, SQLException {
        String code = Character.isUpperCase(barCode.charAt(0)) ? insertion.processCard(barCode) : insertion.processTicket(barCode);
        switch (method) {
            case 1:
                if (oneWayTicket.exitStation(exitStationId, code)) this.openGate();
                else this.closeGate();
                break;
            case 2:
                if (ticket24h.exitStation(exitStationId, code)) this.openGate();
                else this.closeGate();
                break;
            case 3:
                if (prepaidCard.exitStation(exitStationId, code)) this.openGate();
                else this.closeGate();
                break;
        }
    }

    private void openGate() {
        gate.openGate();
    }

    private void closeGate() {
        gate.closeGate();
    }

    /**
     * Calculate fee.
     *
     * @param startStationId the start station id
     * @param endStationId the exit station id
     * @return fee
     * @throws SQLException if there was a problem querying the database
     */
    protected Float calculateFee(int startStationId, int endStationId) throws SQLException {
        float distance = Math.abs(station.getDistance(endStationId) - station.getDistance(startStationId));
        if (distance <= 5) return 1.9f;
        return 1.9f + (float) Math.ceil((distance - 5) / 2) * 0.4f;
    }
}