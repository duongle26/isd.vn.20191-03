package controller;

import model.OneWayTicket;
import view.UI;

import java.sql.*;
import java.util.*;

/**
 * The Class OneWayTicketController.
 */
public class OneWayTicketController extends PlatformController {

	private static OneWayTicketController instance = new OneWayTicketController();

	private StationController station = StationController.getInstance();
	private Statement stmt = DBConnect.getStmt();
	private List<OneWayTicket> listOneWayTicket;
	private OneWayTicket oneWayTicket;
	private UI ui = UI.getInstance();

	public static OneWayTicketController getInstance() {
		return instance;
	}

	/**
	 * Gets the list one way ticket.
	 *
	 * @return the list of one way ticket
	 * @throws SQLException if there was a problem querying the database
	 */
	public List<OneWayTicket> getListOneWayTicket() throws SQLException {
		listOneWayTicket = new ArrayList<OneWayTicket>();
		ResultSet rs = this.stmt.executeQuery("SELECT * FROM ticket_one_way");
		while (rs.next()) {
			listOneWayTicket.add(new OneWayTicket(
					rs.getString(1),
					rs.getString(2),
					rs.getString(3),
					rs.getFloat(4),
					rs.getString(5),
					rs.getInt(6),
					rs.getInt(7),
					rs.getInt(8)
			));
		}
		return this.listOneWayTicket;
	}

	/**
	 * Enter station.
	 *
	 * @param enterStationId the start station id
	 * @param code the code of ticket
	 * @return true, if the passenger can enter, false otherwise
	 * @throws SQLException if there was a problem querying the database
	 */
	public boolean enterStation(int enterStationId, String code) throws SQLException {
		this.setOneWayTicket(code);

		if (this.oneWayTicket.getStatus().equals("using")) {
			ui.setMessage("Invalid one-way ticket");
			ui.setError("Ticket is already in use");
			return false;
		}

		if (this.oneWayTicket.getStatus().equals("expired")) {
			ui.setMessage("Invalid one-way ticket");
			ui.setError("Ticket cannot be used to enter since it was expired");
			return false;
		}

		if (enterStationId >= this.oneWayTicket.getEmbarkStationId() && enterStationId <= this.oneWayTicket.getDisembarkStationId()) {
			this.stmt.executeUpdate("UPDATE ticket_one_way SET status='using', enter_station_id=" + enterStationId + " WHERE code='" + code + "'");
			ui.setMessage(null);
			ui.setError(null);
			return true;
		}

		ui.setMessage("Invalid one-way ticket");
		ui.setError("Ticket can only be used to enter a station between " + station.getStationName(this.oneWayTicket.getEmbarkStationId())
				+ " and " + station.getStationName(this.oneWayTicket.getDisembarkStationId()));
		return false;
	}

	/**
	 * Exit station.
	 *
	 * @param exitStationId the exit station id
	 * @param code the code of ticket
	 * @return true, if the passenger can exit, false otherwise
	 * @throws SQLException if there was a problem querying the database
	 */
	public boolean exitStation(int exitStationId, String code) throws SQLException {
		this.setOneWayTicket(code);

		if (this.oneWayTicket.getStatus().equals("ready")) {
			ui.setMessage("Invalid one-way ticket");
			ui.setError("Ticket is not used to enter a station before");
			return false;
		}

		if (this.oneWayTicket.getStatus().equals("expired")) {
			ui.setMessage("Invalid one-way ticket");
			ui.setError("Ticket cannot be used to exit since it was expired");
			return false;
		}

		Float expectedFee = this.calculateFee(this.oneWayTicket.getEnterStationId(), exitStationId);
		if (this.oneWayTicket.getFare() < expectedFee) {
			ui.setMessage("Invalid one-way ticket");
			ui.setError("Balance: " + this.oneWayTicket.getFare() + " euros\nNot enough balance: Expected " + expectedFee + " euros");
			return false;
		}

		this.stmt.executeUpdate("UPDATE ticket_one_way SET status='expired' WHERE code='" + code + "'");
		ui.setMessage(null);
		ui.setError(null);
		return true;
	}

	/**
	 * Sets the one way ticket.
	 *
	 * @param code the new one way ticket
	 * @throws SQLException if there was a problem querying the database
	 */
	private void setOneWayTicket(String code) throws SQLException {
		ResultSet rs = this.stmt.executeQuery("SELECT * FROM ticket_one_way WHERE code='" + code + "'");
		rs.next();
		this.oneWayTicket = new OneWayTicket(
				rs.getString(1),
				rs.getString(2),
				rs.getString(3),
				rs.getFloat(4),
				rs.getString(5),
				rs.getInt(6),
				rs.getInt(7),
				rs.getInt(8)
		);
	}
}