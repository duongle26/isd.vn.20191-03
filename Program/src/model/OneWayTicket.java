package model;

/**
 * The Class OneWayTicket.
 */
public class OneWayTicket {

	/** The id. */
	private String id;
	
	/** The code. */
	private String code;
	
	/** The bar code. */
	private String barCode;
	
	/** The fare. */
	private Float fare;
	
	/** The status. */
	private String status;
	
	/** The embark station id. */
	private int embarkStationId;
	
	/** The disembark station id. */
	private int disembarkStationId;
	
	/** The enter station id. */
	private int enterStationId;

	/**
	 * Instantiates a new one way ticket.
	 *
	 * @param id the id
	 * @param code the code
	 * @param barCode the bar code
	 * @param fare the fare
	 * @param status the status
	 * @param embarkStationId the embark station id
	 * @param disembarkStationId the disembark station id
	 * @param enterStationId the enter station id
	 */
	public OneWayTicket(String id, String code, String barCode, Float fare, String status, int embarkStationId, int disembarkStationId, int enterStationId) {
		this.id = id;
		this.code = code;
		this.barCode = barCode;
		this.fare = fare;
		this.status = status;
		this.embarkStationId = embarkStationId;
		this.disembarkStationId = disembarkStationId;
		this.enterStationId = enterStationId;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Gets the bar code.
	 *
	 * @return the bar code
	 */
	public String getBarCode() {
		return barCode;
	}

	/**
	 * Gets the fare.
	 *
	 * @return the fare
	 */
	public Float getFare() {
		return fare;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return this.status;
	}

	/**
	 * Gets the embark station id.
	 *
	 * @return the embark station id
	 */
	public int getEmbarkStationId() {
		return embarkStationId;
	}

	/**
	 * Gets the disembark station id.
	 *
	 * @return the disembark station id
	 */
	public int getDisembarkStationId() {
		return disembarkStationId;
	}

	/**
	 * Gets the enter station id.
	 *
	 * @return the enter station id
	 */
	public int getEnterStationId() {
		return enterStationId;
	}
}