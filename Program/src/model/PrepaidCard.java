package model;

/**
 * The Class PrepaidCard.
 */
public class PrepaidCard {
	
	/** The id. */
	private String id;
	
	/** The code. */
	private String code;
	
	/** The bar code. */
	private String barCode;
	
	/** The balance. */
	private Float balance;
	
	/** The status. */
	private String status;
	
	/** The enter station id. */
	private int enterStationId;

	/**
	 * Instantiates a new prepaid card.
	 *
	 * @param id the id
	 * @param code the code
	 * @param barCode the bar code
	 * @param balance the balance
	 * @param status the status
	 * @param enterStationId the enter station id
	 */
	public PrepaidCard(String id, String code, String barCode, Float balance, String status, int enterStationId) {
		this.id = id;
		this.code = code;
		this.barCode = barCode;
		this.balance = balance;
		this.status = status;
		this.enterStationId = enterStationId;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Gets the bar code.
	 *
	 * @return the bar code
	 */
	public String getBarCode() {
		return barCode;
	}

	/**
	 * Gets the balance.
	 *
	 * @return the balance
	 */
	public Float getBalance() {
		return balance;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Gets the enter station id.
	 *
	 * @return the enter station id
	 */
	public int getEnterStationId() {
		return enterStationId;
	}
}