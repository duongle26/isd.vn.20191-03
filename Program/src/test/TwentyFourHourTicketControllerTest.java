package test;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.Timestamp;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import model.Ticket24h;

class TwentyFourHourTicketControllerTest {

	@BeforeEach
	void setUp() throws Exception {
	}


	@Test
	void testSetTwentyFourHourTicket() {
		Ticket24h result = new Ticket24h("TF201912120245", "33d89a87ee9dba49", "qozidjrr", Timestamp.valueOf("2019-12-13T19:42:00"), "ready");
		String a = "ready";
		assertEquals(a, result.getStatus());

	}
}
