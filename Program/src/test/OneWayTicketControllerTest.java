package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import model.OneWayTicket;

class OneWayTicketControllerTest {

	@BeforeEach
	void setUp() throws Exception {
	}


	@Test
	void testOneWayTicket() {
		OneWayTicket rs = new OneWayTicket("OW201912125687", "d804269fb0c6842b", "urjynkgy", 8.72f, "using", 2, 5, 4);
		int a = 5;
		assertEquals(a, rs.getDisembarkStationId());
	}

	@Test
	void testCheckStatusOneWayTicket() {
		OneWayTicket ow = new OneWayTicket("OW201912125687", "d804269fb0c6842b", "urjynkgy", 8.72f, "using", 2, 5, 4);
		String s1 = "using";
		assertEquals(s1, ow.getStatus());
	}
}