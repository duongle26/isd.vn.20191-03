package main;

import hust.soict.se.customexception.InvalidIDException;
import view.UI;

import java.sql.SQLException;

public class Main {
    
    /**
     * The main method.
     *
     * @param args the arguments
     * @throws InvalidIDException when the parameter is invalid
     * @throws SQLException if there was a problem querying the database
     */
    public static void main(String[] args) throws InvalidIDException, SQLException {
        UI ui = UI.getInstance();
        ui.showMenu();
    }
}
